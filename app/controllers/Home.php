<?php
// sesuaikan nama kelas, tetap extends ke Controller
class Home extends Controller
{
  private $key = '269BjWVsCXZUTK9dV8E/9LM/4VwE9Kq9JbiuI2NPt+0=';
  // method default
  public function index()
  {
    $this->view('template/header');
    $this->view('home/index');
    $this->view('template/footer');
  }

  public function book()
  {

    $wahana = "";
    foreach ($_POST['wahana'] as $w) {
      $wahana .= "$w,";
    }
    $wahana = rtrim($wahana, ",");
    $data = ['wahana' => $wahana, 'tanggalMulai' => $_POST['tanggalMulai'], 'jam' => $_POST['jam'], 'tanggalAkhir' => $_POST['tanggalAkhir'], 'namaPIC' => $_POST['namaPIC'], 'jumlahPerson' => $_POST['jumlahPerson'], 'alamat' => $_POST['alamat'],     'telepon' => $_POST['telepon']];
    // print_r($data);

    $result = $this->model('Model_book')->tambah($data);
    $tiket = $this->model('Model_rahasia')->bungkus($result['bookingId']);
    $_SESSION['tiket'] = $tiket;
    if ($result['row'] > 0) {
      header("Location:" . BASEURL . "Confirm/" . $tiket);
    }
  }

  public function test()
  {
    echo "test enkripsi <br>";
    $bungkusan = $this->bungkus('Pikas Lor Wangan Serayu');
    echo $bungkusan . "<br/>";
    echo $this->bongkar($bungkusan) . "<br/>";
  }

  private function bookingDetil($bookingId)
  {
    return $this->model('Model_book')->detail($bookingId);
  }
  // Source encrypte decrypte code: https://bhoover.com/using-php-openssl_encrypt-openssl_decrypt-encrypt-decrypt-data/

  private function bungkus($data)
  {
    // Remove the base64 encoding from our key
    $encryption_key = base64_decode($this->key);
    // Generate an initialization vector
    $iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length('aes-256-cbc'));
    // Encrypt the data using AES 256 encryption in CBC mode using our encryption key and initialization vector.
    $encrypted = openssl_encrypt($data, 'aes-256-cbc', $encryption_key, 0, $iv);
    // The $iv is just as important as the key for decrypting, so save it with our encrypted data using a unique separator (::)
    return base64_encode($encrypted . '::' . $iv);
  }

  private  function bongkar($data)
  {
    // Remove the base64 encoding from our key
    $encryption_key = base64_decode($this->key);
    // To decrypt, split the encrypted data from our IV - our unique separator used was "::"
    list($encrypted_data, $iv) = explode('::', base64_decode($data), 2);
    return openssl_decrypt($encrypted_data, 'aes-256-cbc', $encryption_key, 0, $iv);
  }
}
