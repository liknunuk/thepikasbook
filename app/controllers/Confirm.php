<?php
// sesuaikan nama kelas, tetap extends ke Controller
class Confirm extends Controller
{
  // method default
  public function index($string)
  {
    
    $orderId = $this->model('Model_rahasia')->bongkar($_SESSION['tiket']);
    // echo "OrderId:".$orderId;
    $data['orderDetil'] = $this->model('Model_book')->detail($orderId);
    
    // mpdf header
    // ob_clean();
    // header('Content-type: application/pdf');
    // header('Content-Disposition: inline; filename="' . $data['orderDetil']['bookingId'] . '.pdf"');
    // header('Content-Transfer-Encoding: binary');
    // header('Accept-Ranges: bytes');

    $this->view('home/confirm',$data);
  }

}
