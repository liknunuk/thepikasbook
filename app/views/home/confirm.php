<?php
$tempDir =  __DIR__ . '/mpdfTemp/';
// echo $tempDir;
$bookId = $data['orderDetil']['bookingId'];
$string = "{$data['orderDetil']['wahana']}-{$data['orderDetil']['bookingId']}-{$data['orderDetil']['tgBooking']}-{$data['orderDetil']['namaPIC']}-{$data['orderDetil']['telepon']}";            
$qrcode = "https://nugrahamedia.web.id/qrcode/qr.php?data=".$string;

$pdfDir = dirname(__DIR__,2).'/mpdf';
// echo $pdfDir;
require_once $pdfDir . '/vendor/autoload.php';
$mpdf = new \Mpdf\Mpdf(['format' => 'A5','tempDir'=>$tempDir]);
$BASEURL = BASEURL;
$confirmationMessage = <<<EOL
<img src="$BASEURL/img/thePikas.png" height="75" src="pikas">
<h2>Pikas Adventure Reservation Service</h2>
<hr>
<p>Booking ID Anda adalah : <strong> $bookId </strong></p>
<p>Segera lakukan konfirmasi dengan menghubungi nomor:</p>
<br>

<p>Your Booking ID is : <strong> $bookId </strong></p>
<p>Please confirm immediately by call us at : </p>
<br/>
<img src="$qrcode" alt="$bookId">
<br/>
<a href='$BASEURL'>Home</a>
EOL;
$mpdf->WriteHTML($confirmationMessage);

// Output a PDF file directly to the browser
$mpdf->Output("{$bookId}.pdf", 'I');

?>
