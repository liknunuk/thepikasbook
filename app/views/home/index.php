<div class="container-fluid">
  <div class="row bg-black">
    <div class="col-lg-12">
        <div class="d-flex justify-content-center">
            <img src="<?=BASEURL;?>img/thePikas.png" alt="thepikas" height="60px">
        </div>
        <h4 class="pageTitle">Booking Service</h4>
    </div>    
  </div>
  <div class="row">
    <div class="col-lg-6 mx-auto bg-main">
        <form action="<?=BASEURL;?>Home/book" method="post" class="px-3 py-3 mt-5">
        
        <div class="form-group row mb-0">
            <label for="tpb_service" class="col-sm-4">Layanan <br/><small>Service</small></label>
            <div class="col-sm-8">
            
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="checkbox" name="wahana[]" id="inlineCottage" value="Cottage">
                <label class="form-check-label" for="inlineCottage">Cottage</label>
            </div>

            <div class="form-check form-check-inline">
                <input class="form-check-input" type="checkbox" name="wahana[]" id="inlineOffroad" value="Offroad">
                <label class="form-check-label" for="inlineOffroad">Offroad</label>
            </div>

            <div class="form-check form-check-inline">
                <input class="form-check-input" type="checkbox" name="wahana[]" id="inlineOutbond" value="Outbond">
                <label class="form-check-label" for="inlineOutbond">Outbond</label>
            </div>

            <div class="form-check form-check-inline">
                <input class="form-check-input" type="checkbox" name="wahana[]" id="inlinePaintball" value="Paintball">
                <label class="form-check-label" for="inlinePaintball">Paintball</label>
            </div>

            <div class="form-check form-check-inline">
                <input class="form-check-input" type="checkbox" name="wahana[]" id="inlineRafting" value="Rafting">
                <label class="form-check-label" for="inlineRafting">Rafting</label>
            </div>

            <div class="form-check form-check-inline">
                <input class="form-check-input" type="checkbox" name="wahana[]" id="inlineRestaurant" value="Restaurant">
                <label class="form-check-label" for="inlineRestaurant">Restaurant</label>
            </div>
                <!-- 'Rafting' , 'Paintball' , 'Offroad' , 'Outbond' , 'Cottage' , 'Restaurant' -->
                <!-- <select name="wahana" id="tpb_service" class="form-control">
                <option value="Cottage">Cottage</option>
                <option value="Offroad">Offroad</option>
                <option value="Outbond">Outbond</option>
                <option value="Paintball">Paintball</option>
                <option value="Rafting">Rafting</option>
                <option value="Restaurant">Restaurant</option>
                </select> -->
            </div>
        </div>

        <div class="form-group row mb-0">
            <label for="tpb_tgmulai" class="col-sm-4">Mulai Tanggal <br/><small>Start Date</small></label>
            <div class="col-sm-8">
                <input type="date" name="tanggalMulai" id="tpb_tgmulai" class="form-control" required>
            </div>
        </div>

        <div class="form-group row mb-0">
            <label for="tpb_jam" class="col-sm-4">Jam <br/><small>Time</small></label>
            <div class="col-sm-8">
                <input type="time" name="jam" id="tpb_jam" class="form-control" required>
            </div>
        </div>

        <div class="form-group row mb-0">
            <label for="tpb_tgakhir" class="col-sm-4">Hinggal Tanggal <br/><small>Until</small></label>
            <div class="col-sm-8">
                <input type="date" name="tanggalAkhir" id="tpb_tgakhir" class="form-control">
            </div>
        </div>

        <div class="form-group row mb-0">
            <label for="tpb_peserta" class="col-sm-4">Jumlah Personil<br/><small>Number of persons</small></label>
            <div class="col-sm-8">
                <input type="number" name="jumlahPerson" id="tpb_peserta" class="form-control" value=1 min=1>
            </div>
        </div>

        <div class="form-group row mb-0">
            <label for="tpb_customer" class="col-sm-4">Nama Pemesan <br/><small>Customer Name</small></label>
            <div class="col-sm-8">
                <input type="text" name="namaPIC" id="tpb_customer" class="form-control" required>
            </div>
        </div>

        <!-- bookingId,wahana,tanggalMulai,tanggalAkhir,namaPIC,jumlahPerson,alamat,telepon -->

        <div class="form-group row mb-0">
            <label for="tpb_alamat" class="col-sm-4">Alamat<br><small>Address</small></label>
            <div class="col-sm-8">
                <textarea rows="3" name="alamat" id="tpb_alamat" class="form-control"></textarea>
            </div>
        </div>

        <div class="form-group row mb-0">
            <label for="tpb_telp" class="col-sm-4">Nomor Telepon / HP <br><small>Cellphone Number</small></label>
            <div class="col-sm-8">
                <input type="text" name="telepon" id="tpb_telp" class="form-control">
            </div>
        </div>

        <div class="form-group text-center">
            <input type="submit" value="Submit Booking" class="btn btn-primary">
        </div>
        </form>
    </div>
  </div>
</div>

<?php $this->view('template/bs4js'); ?>
<script>
$('#tpb_tgmulai').on('change', function(){
    $('#tpb_tgakhir').val( this.value );
})
</script>