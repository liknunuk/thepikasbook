<?php
class Model_book
{
    private $table = "booking";
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }

    
    // Basic CRUD OPERATION //
    // bookingId,wahana,tanggalMulai,tanggalAkhir,namaPIC,jumlahPerson,alamat,telepon
    // INSERTION
    

    public function tambah($data){
        
        $bookingId = $this->getBookingId();
        
        
        $sql = "INSERT INTO $this->table SET wahana=:wahana , tanggalMulai=:tanggalMulai , jam=:jam , tanggalAkhir=:tanggalAkhir , namaPIC=:namaPIC , jumlahPerson=:jumlahPerson, alamat=:alamat , telepon=:telepon , bookingId=:bookingId";
        
        $this->db->query($sql);
        $this->db->bind('wahana',$data['wahana']);
        $this->db->bind('tanggalMulai',$data['tanggalMulai']);
        $this->db->bind('jam',$data['jam']);
        $this->db->bind('tanggalAkhir',$data['tanggalAkhir']);
        $this->db->bind('namaPIC',$data['namaPIC']);
        $this->db->bind('jumlahPerson',$data['jumlahPerson']);
        $this->db->bind('alamat',$data['alamat']);
        $this->db->bind('telepon',$data['telepon']);
        $this->db->bind('bookingId',$bookingId);
        
        $this->db->execute();
        
        return array('row'=>$this->db->rowCount() , 'bookingId'=>$bookingId) ;

    }

    // UPDATE
    public function ngubah($data){
        $sql = "UPDATE $this->table SET wahana=:wahana , tanggalMulai=:tanggalMulai , jam=:jam , tanggalAkhir=:tanggalAkhir , namaPIC=:namaPIC , jumlahPerson=:jumlahPIC ,alamat=:alamat , telepon=:telepon WHERE bookingId=:bookingId";
        $this->db->bind('tanggalMulai',$data['tanggalMulai']);
        $this->db->bind('jam',$data['jam']);
        $this->db->bind('tanggalAkhir',$data['tanggalAkhir']);
        $this->db->bind('namaPIC',$data['namaPIC']);
        $this->db->bind('jumlahPerson',$data['jumlahPerson']);
        $this->db->bind('alamat',$data['alamat']);
        $this->db->bind('telepon',$data['telepon']);
        $this->db->bind('bookingId',$data['bookingId']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    // DELETE
    public function sampah($data){
        $sql = "DELETE FROM $this->table WHERE bookingId=:bookingId";
        $this->db-query($sql);
        $this->db->bind('bookingId',$data['bookingId']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    // DISPLAY MULTIPLE
    public function tampil($pn=1){
        $row = ($pn -1 ) * rows;
        $sql = "SELECT * FROM $this->table ORDER BY tgBooking LIMIT $row ," . rows;
        $this->db->query($sql);
        return $this->db->resultSet();
    }

    // DISPLAY SINGULAR
    public function detail($key){
        $sql = "SELECT * FROM $this->table WHERE bookingId=:key ";
        $this->db->query($sql);
        $this->db->bind('key',$key);
        return $this->db->resultOne();
    }

    // CUSTOMIZED QUERY //

    private function getBookingId(){
        $tgl = date('Ymd');
        $jam = date('His');
        $kode= $tgl + $jam;
        
        $bookingId = strtoupper( dechex($kode) ).".".mt_rand(11,99);
        return $bookingId;
    }



}

// QUERY TEMPLATE
